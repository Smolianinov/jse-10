package ru.smolianinov.tm.service;

import ru.smolianinov.tm.entity.Project;
import ru.smolianinov.tm.entity.Task;
import ru.smolianinov.tm.repository.ProjectRepository;
import ru.smolianinov.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * Искать задачи в проекте
     * @param projectId идентификатор проекта
     * @return
     */
    public List<Task> findAddByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAddByProjectId(projectId);
    }

    /**
     * Удалить задачу из проекта
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return
     */
    public Task removeTaskFromProject(final Long projectId, final Long taskId) {
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    /**
     * Добавить задачу в проект
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return
     */
    public Task addTaskToProject(final Long projectId, final Long taskId) {
        final Project project = projectRepository.findById(projectId);
        if (project == null) return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

}
