package ru.smolianinov.tm.repository;

import ru.smolianinov.tm.entity.Project;

import java.util.ArrayList;

import java.util.List;

public class ProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    /**
     * Создать проект
     * @param name имя проекта
     * @return
     */
    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        return project;
    }

    /**
     * Создать проект
     * @param name имя проекта
     * @param description описание проекта
     * @return
     */
    public final Project create(final String name, String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projects.add(project);
        return project;
    }

    /**
     * Изменить проект
     * @param id идентификатор проекта
     * @param name имя проекта
     * @param description описание проекта
     * @return
     */
    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    public final void clear() {
        projects.clear();
    }

    /**
     * Искать проект по порядковому номеру
     * @param index порядковый номер проекта
     * @return
     */
    public final Project findByIndex(int index) {
        if (index < 0 || index > projects.size() - 1) return null;
        return projects.get(index);
    }

    /**
     * Искать проект по имени
     * @param name имя пректа
     * @return
     */
    public final Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        for (final Project project : projects) {
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    /**
     * Искать проект по идентификатору
     * @param id идентификатор проекта
     * @return
     */
    public Project findById(final Long id) {
        if (id == null) return null;
        for (final Project project : projects) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    /**
     * Удалить проект по номеру
     * @param index порядковый номер
     * @return
     */
    public Project removeByIndex(final int index) {
        final Project task = findByIndex(index);
        if (task == null) return null;
        projects.remove(task);
        return task;
    }

    /**
     * Удалить проект по идентификатору
     * @param id идентификатор проекта
     * @return
     */
    public Project removeById(final Long id) {
        final Project task = findById(id);
        if (task == null) return null;
        projects.remove(task);
        return task;
    }

    /**
     * Удалить проект по имени
     * @param name имя пректа
     * @return
     */
    public Project removeByName(final String name) {
        final Project task = findByName(name);
        if (task == null) return null;
        projects.remove(task);
        return task;
    }

    public List<Project> findAll() {
        return projects;
    }

    public static void main(String[] args) {
        final ProjectRepository projectRepository = new ProjectRepository();
        projectRepository.create("DEMO");
        projectRepository.create("TEST");
        System.out.println(projectRepository.findAll());
        projectRepository.clear();
        System.out.println(projectRepository.findAll());
    }

}
